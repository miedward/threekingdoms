--
-- Assumes you have the following aset parameters set
--MOB_example   look_monster_pref: MOB_            look_monster_suff: <ESC>[0m
--PLAYER_example   look_player_pref: PLAYER_          look_player_suff: <ESC>[0m
--
local coreBot = {}

-- List of guild plugins which contain valid corpse triggers calling broadcast

stepNumber = 1
botWasMoving = false
botMoving = false
numberMobs = 0
numberPlayers = 0
fighting = false

playerList = {}

partyMembers = {}
numberPartyMembers = 0
mobList = {}

ignoreList = {"anuk, hsima's siberian husky", "massive wandering spider"}

numberIgnores = #ignoreList

brokenMobCorrections = {}

botDirs = {}


debug = false

function coreBot.Set (list)
  local set = {}
  for _, l in ipairs(list) do set[l] = true end
  return set
end

kill_trigger_plugin_ids = coreBot.Set({"e124f520f44f16b0675a1009",
							"b267d5e95e94e68e3c064a6f",
							"31895c2e9fa2fe8ceeb21b81",
							"b269d5e95e94e69e3c069a6f",
							"31895c2e9fa2fe8ceeb21b82"
						})

function coreBot.moveBot()
	if (botMoving) then
		Note("Bot Moving")
		if (botDirs[stepNumber] == "stopBot") then
			coreBot.stopBot()
			coreBot.resetBot()
		else
			SendNoEcho(botDirs[stepNumber])
			stepNumber=stepNumber+1
		end
	else
		if (numberPlayers > 0) then
			if (not(coreBot.allPartyMembers())) then
				Note(numberPlayers, 
				" Players are here, continuing.")
				coreBot.clearRoomData()
				coreBot.startBot()
				return
			end
		end
		if ( not(fighting) and numberMobs > 0) then	
			startMatch, endMatch, someStuff, otherStuff =
				string.find(mobList[1], "broodlings")
			if (debug) then
				Note("BroodlingCheck: ", mobList[1],
					startMatch,
				 endMatch, someStuff, otherStuff)
			end 
			if ( numberMobs == 1 and startMatch ~= nil ) then
			
				coreBot.clearRoomData()
				coreBot.startBot()
				return
			end

			Note(numberMobs, 
			" mobs are here, no Players, fight something.")
		coreBot.stopBot()
		coreBot.killSomething()
		elseif ( not(fighting) and numberMobs == 0) then
			Note("Mobs probably left, moving on.")
			coreBot.clearRoomData()
			coreBot.startBot()
		else
			Note("Bot is probably fighting something.")
		end
	end
end

function coreBot.moveBlocked()
	stepNumber=stepNumber-1
	coreBot.stopBot()
	SendNoEcho("glance")
end


function coreBot.somethingDied()
	mobList[numberMobs] = nil
	numberMobs = numberMobs - 1
	fighting = false
	if (numberMobs <= 0) then
		coreBot.startBot()
		if (numberMobs < 0) then
			numberMobs = 0;
			Note("Fixing Negative NumberMobs.")
		end
	else
		coreBot.clearRoomData()
		EnableTimer("genBot_timer", true)
		SendNoEcho("glance")
	end
end

function coreBot.startBot()
	EnableTimer("genBot_timer", true)
	coreBot.clearRoomData()
	coreBot.startMoving()
end

function coreBot.stopBot()
	EnableTimer("genBot_timer", false)
	coreBot.stopMoving()
end

function coreBot.stopMoving()
	botMoving=false
end

function coreBot.startMoving()
	botMoving=true
end

function coreBot.addMob(mobName)
	local startMatch = nil
	local endMatch = nil
--Strip off Articles - An
	if (debug) then
		Note(mobName)
	end
	startMatch = nil
	endMatch = nil
	realName = nil
	if (string.match(mobName, "(.+)%.") ~= nil) then
		mobName = string.match(mobName, "(.+)%.")
	end
	startMatch, endMatch = 
		string.find(mobName, "An%s+")
	if (debug) then
		Note(startMatch, endMatch, realName)
	end
	if (endMatch ~= nil) then
		mobName = string.sub(mobName, endMatch+1)
	end
--Strip off Articles - A
	if (debug) then
		Note(mobName)
	end
	startMatch = nil
	endMatch = nil
	realName = nil
	startMatch, endMatch = 
		string.find(mobName, "A%s+")
	if (debug) then
		Note(startMatch, endMatch, realName)
	end
	if (endMatch ~= nil) then
		mobName = string.sub(mobName, endMatch+1)
	end
--Strip off Articles - The
	startMatch = nil
	endMatch = nil
	realName = nil
	if (debug) then
		Note(mobName)
	end
	startMatch, endMatch = 
		string.find(mobName, "The%s+")
	if (debug) then
		Note(startMatch, endMatch, realName)
	end
	if (endMatch ~= nil) then
		mobName = string.sub(mobName, endMatch+1)
	end
	if (debug) then
		Note(mobName)
	end
--Convert to Lower Case
	mobName = mobName:lower()
	if (debug) then
		Note(mobName)
	end
--Catch Stacks of mobs
	startMatch = nil
	endMatch = nil
	stackNumber = nil
	startMatch, endMatch, stackNumber = 
		string.find(mobName, "%s+{(%d+)}")
	if (debug) then
		Note(startMatch, endMatch, stackNumber)
	end
	if (startMatch ~= nil) then
		mobName = string.sub(mobName, 1, startMatch - 1)
	end
	if (debug) then
		Note(mobName)
	end
--Catch mobs who are already attacking
	startMatch = nil
	endsMatch = nil
	startMatch, endMatch = string.find(mobName, "attacking you!")
	if (debug) then
		Note(startMatch, endMatch)
	end
	if (startMatch ~= nil) then
		mobName = string.sub(mobName, 1, startMatch - 2)
		fighting = true
	end
	if (debug) then
		Note(mobName)
	end
-- Fudge broken mobs --
	if (brokenMobCorrections[mobName] ~= nil) then
		mobName = brokenMobCorrections[mobName]
	end
	if (debug) then
		Note(mobName)
	end
	if (stackNumber ~= nil) then
		for i=numberMobs,numberMobs+stackNumber do
			mobList[i]= mobName
		end
		numberMobs = numberMobs + stackNumber
	else
		numberMobs = numberMobs + 1
		mobList[numberMobs] = mobName
	end
end

function coreBot.addPlayer(playerName)
    if (string.match(playerName, "(.+)%.") ~= nil) then
		playerName = string.match(playerName, "(.+)%.")
	end
	numberPlayers = numberPlayers + 1
	playerList[numberPlayers] = playerName
end

function coreBot.addPartyMember(playerName)
	numberPartyMembers = numberPartyMembers + 1
	partyMembers[numberPartyMembers] = playerName
end

function coreBot.killSomething()
	okayMob = "None."
	if ( mobList == nil ) then 
		Note("mobList is Nil")
		return
	 end
	if ( ignoreList == nil ) then 
		Note("ignoreList is Nil")
		return
	 end
	fighting = true
	for i= 1, numberMobs do
		ignoreThis = false
		for j= 1, numberIgnores do
			startMatch, endMatch = 
				string.find(mobList[i], ignoreList[j])
			if (debug) then
				Note("IgnoreCheck: ", 
					mobList[i], ignoreList[j], startMatch)
			end
			if (startMatch ~= nil) then
				ignoreThis = true
			end
		end
		if (not(ignoreThis)) then okayMob = mobList[i] end
	end
	-- This will kill the last non ignored mob --
	if (okayMob ~= "None.") then
		SendNoEcho("kill ", okayMob)
    --		SendNoEcho("emote Killing:  ", okayMob) --
	else
		coreBot.clearRoomData()
		coreBot.startBot()
	end
end

function coreBot.printBotStatus()
	Note("Fighting: ", fighting)
	Note("There are ",numberMobs, " mobs here.")
	Note("Moblist:")
	for i=1, numberMobs do
		Note(mobList[i],"\n")
	end
	Note("There are ",numberPlayers, " players here.")
	Note("Playerlist:")
	for i=1, numberPlayers do
		Note(playerList[i],"\n")
	end
	Note("PartyMembers:")

	for i=1, numberPartyMembers do
		Note(partyMembers[i],"\n")
	end

	Note("Are all the players party members? ", 
		coreBot.allPartyMembers())
end

function coreBot.clearRoomData()
	numberMobs = 0
	numberPlayers = 0
	fighting = false
	botMoving = false
	mobList = {}
	playerList = {}
end

function coreBot.resetBot()
	Note("Reseting genBot.")

	stepNumber = 1
	botWasMoving = false
	botMoving = false

	numberMobs = 0
	numberPlayers = 0
	fighting = false

	playerList = {}
	partyMembers = {}
	numberPartyMembers = 0

	mobList = {}

	ignoreList = {"anuk, hsima's siberian husky"}
	numberIgnores = #ignoreList

	brokenMobCorrections = {}
	
	botLocation = "None"
	
	botDirs = {}
	coreBot.stopBot()
end

function coreBot.allPartyMembers()
	local numMatches = 0
	if numberPartyMembers == 0 then
		return false
	end
	for i= 1, numberPlayers do
		ignoreThis = false
		for j= 1, numberPartyMembers do
			startMatch, endMatch = 
				string.find(playerList[i], partyMembers[j])
			Note("IgnoreCheck: ", 
				playerList[i], partyMembers[j], startMatch)
			if (startMatch ~= nil) then
				numMatches = numMatches+1
			else
				return false
			end
		end
	end
	if (numMatches == numberPartyMembers) then
		return true
	else
		return false
	end
end

function coreBot.addIgnoreListItem(ignoreThis)
	table.insert(ignoreList, ignoreThis)
	numberIgnores = #ignoreList
end

function coreBot.addMobCorrectionString(brokenString, workingString)
	brokenMobCorrections[brokenString] = workingString
end

function coreBot.addBotDirs ( tableDirs )
	botDirs = tableDirs
end

return coreBot

